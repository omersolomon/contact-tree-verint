import { VerintContactTreePage } from './app.po';

describe('verint-contact-tree App', () => {
  let page: VerintContactTreePage;

  beforeEach(() => {
    page = new VerintContactTreePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
